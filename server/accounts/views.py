import json
from dataclasses import asdict

from django.contrib.auth import login
from rest_framework import permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,)
from knox.views import LoginView as KnoxLoginView
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from utils.parsers import ResponseObjCreator


class LoginView(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['username', 'password'],
            properties={
                'username':
                    openapi.Schema(type=openapi.TYPE_STRING),
                'password':
                    openapi.Schema(type=openapi.TYPE_STRING,
                                   format=openapi.FORMAT_PASSWORD)
            },
        ),
        operation_id='login',
        responses={
            200:
                openapi.Response(
                    description='Returns token',
                    examples={
                        "application/json": {
                            "code": None,
                            "success": True,
                            "data": {
                                "expiry":
                                    "2024-04-07T20:38:22.559572+08:00",
                                "token":
                                    "801e8abef8bb9a4e57b34f229291c1eedade8ffb7033ff0aae6a791f91ec5fdb",
                            },
                            "message": ""
                        }
                    }),
            400:
                openapi.Response(
                    description='Unable to log in',
                    examples={
                        "application/json": {
                            "code":
                                None,
                            "success":
                                False,
                            "data": {},
                            "message":
                                "{\"non_field_errors\": [\"Unable to log in with provided credentials.\"]}"
                        }
                    })
        })
    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        if not serializer.is_valid():
            res = ResponseObjCreator().create(success=False,
                                              message=json.dumps(
                                                  serializer.errors,
                                                  ensure_ascii=False))
            return Response(asdict(res), status=HTTP_400_BAD_REQUEST)

        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginView, self).post(request, format=None)

    def get_post_response_data(self, request, token, instance):
        UserSerializer = self.get_user_serializer_class()

        data = {
            'expiry': self.format_expiry_datetime(instance.expiry),
            'token': token
        }
        if UserSerializer is not None:
            data["user"] = UserSerializer(request.user,
                                          context=self.get_context()).data

        res = ResponseObjCreator().create(success=True, message='')
        res = asdict(res)
        res.update({'data': data})
        return res
