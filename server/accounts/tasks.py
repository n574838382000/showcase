from celery import shared_task

from django.core import management
from django.utils import timezone
from knox.models import AuthToken


@shared_task(name='delete_expired_django_session')
def delete_expired_django_session():
    management.call_command(command_name='clearsessions')


@shared_task(name='delete_expired_token_objs')
def delete_expired_token_objs():
    '''Delete expired AuthToken
    '''

    token_objs = AuthToken.objects.all()
    for token_obj in token_objs:
        if token_obj.expiry:
            if token_obj.expiry < timezone.now():
                token_obj.delete()
