from dataclasses import asdict
import json

from django.shortcuts import get_object_or_404
from rest_framework import mixins, viewsets
from rest_framework.decorators import (
    action,)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST,
    HTTP_500_INTERNAL_SERVER_ERROR,
)
from knox.auth import TokenAuthentication
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from shows.models import (
    Show,
    ShowInfo,
)
from shows.serializers import (
    ShowSerializer,
    ShowInfoSerializer,
)
from shows.tasks import update_show_data
from utils.pagination import StandardPagination
from utils.parsers import (
    ResponseObjCreator,)


class ShowViewset(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet,
):
    '''Show viewset
    '''
    authentication_classes = (TokenAuthentication,)
    queryset = (Show.objects.all().order_by('id'))
    serializer_class = ShowSerializer
    pagination_class = StandardPagination

    def get_permissions(self):
        if self.action in ('partial_update', 'update', 'destroy'):
            self.permission_classes = (IsAuthenticated,)
        return [permission() for permission in self.permission_classes]

    @swagger_auto_schema(responses={
        200:
            openapi.Response(
                description='List a queryset.',
                examples={
                    "application/json": {
                        "code": None,
                        "success": True,
                        "data": {
                            "count":
                                422,
                            "next":
                                "http://54.250.115.129:8000/operation/?page=2",
                            "previous":
                                None,
                            "results": [{
                                "id":
                                    "645357a031bef61dcaf57d5c",
                                "created_at":
                                    "2024-04-01T16:03:37.499588+08:00",
                                "updated_at":
                                    "2024-04-01T16:03:37.499623+08:00",
                                "version":
                                    "1.4",
                                "title":
                                    "★音樂進站-讓捷運站成為你的舞台吧★邀你來表演！",
                                "category":
                                    "1",
                                "show_unit":
                                    "",
                                "discount_info":
                                    "",
                                "description_filter_html":
                                    "音樂進站-捷運音樂演出計畫\r\n\r\n邀你來表演\r\n\r\n讓捷運站成為你的舞台吧\r\n\r\n想辦成果發表會？音樂會？演奏會？讓我們幫你實現！\r\n\r\n除了場地”免費”！還贈送捷運一日票！\r\n\r\n萬眾矚目~為期兩週登上全線捷運站月台電視 !\r\n\r\n學生團體再加碼「服務學習3小時證明」！\r\n\r\n歡迎擁有「樂器演奏或合唱團表演」才華的你，與我們連繫！\r\n\r\n台北捷運音樂進站計畫等你來申請~\r\n\r\n詳情可參閱台北捷運官網\r\n\r\nhttps://www.metro.taipei/News_Content.aspx?n=320CF0D294FF9489&sms=EA56BA3CA296DDF6&s=E7002ECCF18E05F0\r\n\r\n願望成真專線：(02)2536-3001 分機8365李小姐、8168林小姐",
                                "image_url":
                                    "https://cloud.culture.tw/e_new_upload/cms/image/A0/B0/C0/D-753/E-166/F-566/62c8047f-2796-4065-8a66-5d6ce6dabbfd.jpg",
                                "web_sales":
                                    "",
                                "source_web_promote":
                                    "",
                                "comment":
                                    "免費",
                                "edit_modify_date":
                                    "2024-01-04T14:14:10+08:00",
                                "source_web_name":
                                    "林上琦",
                                "start_date":
                                    "2024-04-06T00:00:00+08:00",
                                "end_date":
                                    "2024-12-29T00:00:00+08:00",
                                "hit_rate":
                                    1606,
                                "master_unit": ["台北捷運公司"],
                                "sub_unit": [],
                                "support_unit": [],
                                "other_unit": []
                            }]
                        },
                        "message": ""
                    }
                })
    },)
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(responses={
        200:
            openapi.Response(
                description='Retrieve a model instance.',
                examples={
                    "application/json": {
                        "id":
                            "645357a031bef61dcaf57d5c",
                        "created_at":
                            "2024-04-01T16:03:37.499588+08:00",
                        "updated_at":
                            "2024-04-01T16:03:37.499623+08:00",
                        "version":
                            "1.4",
                        "title":
                            "★音樂進站-讓捷運站成為你的舞台吧★邀你來表演！",
                        "category":
                            "1",
                        "show_unit":
                            "",
                        "discount_info":
                            "",
                        "description_filter_html":
                            "音樂進站-捷運音樂演出計畫\r\n\r\n邀你來表演\r\n\r\n讓捷運站成為你的舞台吧\r\n\r\n想辦成果發表會？音樂會？演奏會？讓我們幫你實現！\r\n\r\n除了場地”免費”！還贈送捷運一日票！\r\n\r\n萬眾矚目~為期兩週登上全線捷運站月台電視 !\r\n\r\n學生團體再加碼「服務學習3小時證明」！\r\n\r\n歡迎擁有「樂器演奏或合唱團表演」才華的你，與我們連繫！\r\n\r\n台北捷運音樂進站計畫等你來申請~\r\n\r\n詳情可參閱台北捷運官網\r\n\r\nhttps://www.metro.taipei/News_Content.aspx?n=320CF0D294FF9489&sms=EA56BA3CA296DDF6&s=E7002ECCF18E05F0\r\n\r\n願望成真專線：(02)2536-3001 分機8365李小姐、8168林小姐",
                        "image_url":
                            "https://cloud.culture.tw/e_new_upload/cms/image/A0/B0/C0/D-753/E-166/F-566/62c8047f-2796-4065-8a66-5d6ce6dabbfd.jpg",
                        "web_sales":
                            "",
                        "source_web_promote":
                            "",
                        "comment":
                            "免費",
                        "edit_modify_date":
                            "2024-01-04T14:14:10+08:00",
                        "source_web_name":
                            "林上琦",
                        "start_date":
                            "2024-04-06T00:00:00+08:00",
                        "end_date":
                            "2024-12-29T00:00:00+08:00",
                        "hit_rate":
                            1606,
                        "master_unit": ["台北捷運公司"],
                        "sub_unit": [],
                        "support_unit": [],
                        "other_unit": []
                    }
                })
    },)
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        serializer = ShowSerializer(instance,
                                    data=request.data,
                                    partial=partial)
        if not serializer.is_valid():
            res = ResponseObjCreator().create(success=False,
                                              message=json.dumps(
                                                  serializer.errors,
                                                  ensure_ascii=False))
            return Response(asdict(res), status=HTTP_400_BAD_REQUEST)

        try:
            self.perform_update(serializer)
        except Exception as err:
            res = ResponseObjCreator().create(success=False,
                                              message=err.args[0])
            return Response(
                asdict(res),
                status=HTTP_500_INTERNAL_SERVER_ERROR,
            )

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        res = ResponseObjCreator().create(success=True)
        res = asdict(res)
        res.update({'data': serializer.data})
        return Response(res, status=HTTP_200_OK)

    @swagger_auto_schema(
        methods=['get'],
        auto_schema=None,
    )
    @action(
        methods=['get'],
        detail=False,
    )
    def sync(self, request):
        update_show_data.delay()

        res = ResponseObjCreator().create(
            success=True,
            message='Received update-show-data request successfully')
        return Response(
            asdict(res),
            status=HTTP_200_OK,
        )


class ShowInfoViewset(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet,
):
    '''ShowInfo viewset
    '''
    authentication_classes = (TokenAuthentication,)
    queryset = (ShowInfo.objects.all().order_by('id'))
    serializer_class = ShowInfoSerializer
    pagination_class = StandardPagination

    def get_permissions(self):
        if self.action in ('partial_update', 'update', 'destroy'):
            self.permission_classes = (IsAuthenticated,)
        return [permission() for permission in self.permission_classes]

    def get_queryset(self):
        show_id = self.kwargs.get('show_id')
        show_obj = get_object_or_404(Show, id=show_id)
        return self.queryset.filter(show=show_obj)

    @swagger_auto_schema(responses={
        200:
            openapi.Response(
                description='List a queryset.',
                examples={
                    "application/json": {
                        "code": None,
                        "success": True,
                        "data": {
                            "count":
                                78,
                            "next":
                                "http://54.250.115.129:8000/operation/645357a031bef61dcaf57d5c/show-info/?page=2",
                            "previous":
                                None,
                            "results": [{
                                "id":
                                    "645357a031bef61dcaf57d5c0",
                                "show":
                                    "★音樂進站-讓捷運站成為你的舞台吧★邀你來表演！",
                                "created_at":
                                    "2024-04-01T16:03:37.502582+08:00",
                                "updated_at":
                                    "2024-04-01T16:03:37.502596+08:00",
                                "time":
                                    "2024-04-06T00:00:00+08:00",
                                "location":
                                    "臺北市中山站 4 號出口心中山舞臺、東區地下街第 2 廣場、大安森林公園站陽光大廳、松山站穹頂廣場、新店站廣場",
                                "location_name":
                                    "台北捷運音樂進站",
                                "on_sales":
                                    False,
                                "price":
                                    "",
                                "latitude":
                                    None,
                                "longitude":
                                    None,
                                "end_time":
                                    "2024-04-06T00:00:00+08:00"
                            }]
                        },
                        "message": ""
                    }
                })
    },)
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(responses={
        200:
            openapi.Response(
                description='Retrieve a model instance.',
                examples={
                    "application/json": {
                        "id":
                            "645357a031bef61dcaf57d5c0",
                        "show":
                            "★音樂進站-讓捷運站成為你的舞台吧★邀你來表演！",
                        "created_at":
                            "2024-04-01T16:03:37.502582+08:00",
                        "updated_at":
                            "2024-04-01T16:03:37.502596+08:00",
                        "time":
                            "2024-04-06T00:00:00+08:00",
                        "location":
                            "臺北市中山站 4 號出口心中山舞臺、東區地下街第 2 廣場、大安森林公園站陽光大廳、松山站穹頂廣場、新店站廣場",
                        "location_name":
                            "台北捷運音樂進站",
                        "on_sales":
                            False,
                        "price":
                            "",
                        "latitude":
                            None,
                        "longitude":
                            None,
                        "end_time":
                            "2024-04-06T00:00:00+08:00"
                    }
                })
    },)
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        serializer = ShowInfoSerializer(instance,
                                        data=request.data,
                                        partial=partial)
        if not serializer.is_valid():
            res = ResponseObjCreator().create(success=False,
                                              message=json.dumps(
                                                  serializer.errors,
                                                  ensure_ascii=False))
            return Response(asdict(res), status=HTTP_400_BAD_REQUEST)

        try:
            self.perform_update(serializer)
        except Exception as err:
            res = ResponseObjCreator().create(success=False,
                                              message=err.args[0])
            return Response(
                asdict(res),
                status=HTTP_500_INTERNAL_SERVER_ERROR,
            )

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        res = ResponseObjCreator().create(success=True)
        res = asdict(res)
        res.update({'data': serializer.data})
        return Response(res, status=HTTP_200_OK)
