from datetime import datetime
import zoneinfo

from rest_framework import serializers

from shows.models import (
    Organizer,
    Show,
    ShowInfo,
)


class OrganizerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organizer
        fields = '__all__'


class ShowSerializer(serializers.ModelSerializer):
    id = serializers.CharField(read_only=True)

    class Meta:
        model = Show
        fields = '__all__'

    def validate_category(self, value):
        """
        Check that the category is valid.
        """
        if value not in Show.Category.values:
            raise serializers.ValidationError('Not a valid category')
        return value

    def to_internal_value(self, data):

        def get_organizer_id_list(organizer_name_list):
            organizer_id_list = []
            if not organizer_name_list:
                return organizer_id_list

            for organizer_name in organizer_name_list:
                organizer_obj, _ = Organizer.objects.get_or_create(
                    name=organizer_name)
                organizer_id_list.append(organizer_obj.id)

            return organizer_id_list

        taipei_tz = zoneinfo.ZoneInfo('Asia/Taipei')

        # NOTE: master_unit
        if data.get('master_unit'):
            master_organizer_id_list = get_organizer_id_list(
                organizer_name_list=data.get('master_unit'))
            data.update({'master_unit': master_organizer_id_list})

        # NOTE: sub_unit
        if data.get('sub_unit'):
            sub_organizer_id_list = get_organizer_id_list(
                organizer_name_list=data.get('sub_unit'))
            data.update({'sub_unit': sub_organizer_id_list})

        # NOTE: support_unit
        if data.get('support_unit'):
            support_organizer_id_list = get_organizer_id_list(
                organizer_name_list=data.get('support_unit'))
            data.update({'support_unit': support_organizer_id_list})

        # NOTE: other_unit
        if data.get('other_unit'):
            other_organizer_id_list = get_organizer_id_list(
                organizer_name_list=data.get('other_unit'))
            data.update({'other_unit': other_organizer_id_list})

        # NOTE: edit_modify_date
        if data.get('edit_modify_date'):
            edit_modify_date = datetime.strptime(data['edit_modify_date'],
                                                 '%Y/%m/%d %H:%M:%S')
            edit_modify_date = edit_modify_date.replace(tzinfo=taipei_tz)
            data.update({'edit_modify_date': edit_modify_date})

        # NOTE: start_date
        if data.get('start_date'):
            start_date = datetime.strptime(data['start_date'], '%Y/%m/%d')
            start_date = start_date.replace(tzinfo=taipei_tz)
            data.update({'start_date': start_date})

        # NOTE: end_date
        if data.get('end_date'):
            end_date = datetime.strptime(data['end_date'], '%Y/%m/%d')
            end_date = end_date.replace(tzinfo=taipei_tz)
            data.update({'end_date': end_date})

        return super().to_internal_value(data)

    def to_representation(self, instance):
        ret = super().to_representation(instance)

        # NOTE: master_unit
        ret['master_unit'] = []
        for item in instance.master_unit.all():
            org = OrganizerSerializer(item).data['name']
            ret['master_unit'].append(org)

        # NOTE: sub_unit
        ret['sub_unit'] = []
        for item in instance.sub_unit.all():
            org = OrganizerSerializer(item).data['name']
            ret['sub_unit'].append(org)

        # NOTE: support_unit
        ret['support_unit'] = []
        for item in instance.support_unit.all():
            org = OrganizerSerializer(item).data['name']
            ret['support_unit'].append(org)

        # NOTE: other_unit
        ret['other_unit'] = []
        for item in instance.other_unit.all():
            org = OrganizerSerializer(item).data['name']
            ret['other_unit'].append(org)

        return ret

    def update(self, instance, validated_data):
        master_unit = validated_data.pop('master_unit', None)
        sub_unit = validated_data.pop('sub_unit', None)
        support_unit = validated_data.pop('support_unit', None)
        other_unit = validated_data.pop('other_unit', None)
        if master_unit:
            instance.master_unit.clear()
            instance.master_unit.set(master_unit)
        if sub_unit:
            instance.sub_unit.clear()
            instance.sub_unit.set(sub_unit)
        if support_unit:
            instance.support_unit.clear()
            instance.support_unit.set(support_unit)
        if other_unit:
            instance.other_unit.clear()
            instance.other_unit.set(other_unit)

        instance.version = validated_data.get('version', instance.version)
        instance.title = validated_data.get('title', instance.title)
        instance.category = validated_data.get('category', instance.category)
        instance.show_unit = validated_data.get('show_unit',
                                                instance.show_unit)
        instance.discount_info = validated_data.get('discount_info',
                                                    instance.discount_info)
        instance.description_filter_html = validated_data.get(
            'description_filter_html', instance.description_filter_html)
        instance.image_url = validated_data.get('image_url',
                                                instance.image_url)
        instance.web_sales = validated_data.get('web_sales',
                                                instance.web_sales)
        instance.source_web_promote = validated_data.get(
            'source_web_promote', instance.source_web_promote)
        instance.comment = validated_data.get('comment', instance.comment)
        instance.edit_modify_date = validated_data.get(
            'edit_modify_date', instance.edit_modify_date)
        instance.source_web_name = validated_data.get('source_web_name',
                                                      instance.source_web_name)
        instance.start_date = validated_data.get('start_date',
                                                 instance.start_date)
        instance.end_date = validated_data.get('end_date', instance.end_date)
        instance.hit_rate = validated_data.get('hit_rate', instance.hit_rate)

        instance.save()

        return instance


class ShowRawSerializer(serializers.Serializer):
    id = serializers.CharField(
        required=True,
        max_length=30,
    )
    version = serializers.CharField(
        required=True,
        max_length=5,
    )
    title = serializers.CharField(
        required=True,
        max_length=100,
    )
    category = serializers.CharField(
        required=True,
        max_length=1,
    )
    show_unit = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=200,
    )
    discount_info = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=1000,
    )
    description_filter_html = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=3000,
    )
    image_url = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=200,
    )
    master_unit = serializers.ListField(
        child=serializers.IntegerField(min_value=1),
        default=[],
        allow_empty=True,
    )
    sub_unit = serializers.ListField(
        child=serializers.IntegerField(min_value=1),
        default=[],
        allow_empty=True,
    )
    support_unit = serializers.ListField(
        child=serializers.IntegerField(min_value=1),
        default=[],
        allow_empty=True,
    )
    other_unit = serializers.ListField(
        child=serializers.IntegerField(min_value=1),
        default=[],
        allow_empty=True,
    )
    web_sales = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=200,
    )
    source_web_promote = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=200,
    )
    comment = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=300,
    )
    edit_modify_date = serializers.DateTimeField(allow_null=True,)
    source_web_name = serializers.CharField(
        required=True,
        max_length=30,
    )
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()
    hit_rate = serializers.IntegerField(
        allow_null=True,
        min_value=0,
    )

    def validate_category(self, value):
        """
        Check that the category is valid.
        """
        if value not in Show.Category.values:
            raise serializers.ValidationError('Not a valid category')
        return value

    def to_internal_value(self, data):

        def get_organizer_id_list(organizer_name_list):
            organizer_id_list = []

            for organizer_name in organizer_name_list:
                organizer_obj, _ = Organizer.objects.get_or_create(
                    name=organizer_name)
                organizer_id_list.append(organizer_obj.id)

            return organizer_id_list

        taipei_tz = zoneinfo.ZoneInfo('Asia/Taipei')

        # NOTE: master_unit
        master_organizer_id_list = get_organizer_id_list(
            organizer_name_list=data['masterUnit'])

        # NOTE: sub_unit
        sub_organizer_id_list = get_organizer_id_list(
            organizer_name_list=data['subUnit'])

        # NOTE: support_unit
        support_organizer_id_list = get_organizer_id_list(
            organizer_name_list=data['supportUnit'])

        # NOTE: other_unit
        other_organizer_id_list = get_organizer_id_list(
            organizer_name_list=data['otherUnit'])

        # NOTE: edit_modify_date
        edit_modify_date = None
        if data['editModifyDate']:
            edit_modify_date = datetime.strptime(data['editModifyDate'],
                                                 '%Y/%m/%d %H:%M:%S')
            edit_modify_date = edit_modify_date.replace(tzinfo=taipei_tz)

        # NOTE: start_date
        start_date = datetime.strptime(data['startDate'], '%Y/%m/%d')
        start_date = start_date.replace(tzinfo=taipei_tz)

        # NOTE: end_date
        end_date = datetime.strptime(data['endDate'], '%Y/%m/%d')
        end_date = end_date.replace(tzinfo=taipei_tz)

        return {
            'id': data['UID'],
            'version': data['version'],
            'title': data['title'],
            'category': data['category'],
            'show_unit': data['showUnit'],
            'discount_info': data['discountInfo'],
            'description_filter_html': data['descriptionFilterHtml'],
            'image_url': data['imageUrl'],
            'master_unit': master_organizer_id_list,
            'sub_unit': sub_organizer_id_list,
            'support_unit': support_organizer_id_list,
            'other_unit': other_organizer_id_list,
            'web_sales': data['webSales'],
            'source_web_promote': data['sourceWebPromote'],
            'comment': data['comment'],
            'edit_modify_date': edit_modify_date,
            'source_web_name': data['sourceWebName'],
            'start_date': start_date,
            'end_date': end_date,
            'hit_rate': data['hitRate'],
        }

    def create(self, validated_data):
        master_unit = validated_data.pop('master_unit', None)
        sub_unit = validated_data.pop('sub_unit', None)
        support_unit = validated_data.pop('support_unit', None)
        other_unit = validated_data.pop('other_unit', None)

        show_obj = Show.objects.create(**validated_data)

        if master_unit:
            show_obj.master_unit.set(master_unit)
        if sub_unit:
            show_obj.sub_unit.set(sub_unit)
        if support_unit:
            show_obj.support_unit.set(support_unit)
        if other_unit:
            show_obj.other_unit.set(other_unit)

        return show_obj


class ShowInfoSerializer(serializers.ModelSerializer):
    id = serializers.CharField(read_only=True)
    show = serializers.CharField(read_only=True)

    class Meta:
        model = ShowInfo
        fields = '__all__'

    def to_internal_value(self, data):
        taipei_tz = zoneinfo.ZoneInfo('Asia/Taipei')

        # NOTE: time
        if data.get('time'):
            time = datetime.strptime(data['time'], '%Y/%m/%d %H:%M:%S')
            time = time.replace(tzinfo=taipei_tz)
            data.update({'time': time})

        # NOTE: end_time
        if data.get('end_time'):
            end_time = datetime.strptime(data['end_time'], '%Y/%m/%d %H:%M:%S')
            end_time = end_time.replace(tzinfo=taipei_tz)
            data.update({'end_time': end_time})

        return super().to_internal_value(data)

    def update(self, instance, validated_data):
        instance.time = validated_data.get('time', instance.time)
        instance.location = validated_data.get('location', instance.location)
        instance.location_name = validated_data.get('location_name',
                                                    instance.location_name)
        instance.on_sales = validated_data.get('on_sales', instance.on_sales)
        instance.price = validated_data.get('price', instance.price)
        instance.latitude = validated_data.get('latitude', instance.latitude)
        instance.longitude = validated_data.get('longitude',
                                                instance.longitude)
        instance.end_time = validated_data.get('end_time', instance.end_time)

        instance.save()

        return instance


class ShowInfoRawSerializer(serializers.Serializer):
    id = serializers.CharField(
        required=True,
        max_length=35,
    )
    show_id = serializers.CharField(
        required=True,
        max_length=30,
    )
    time = serializers.DateTimeField(required=True)
    location = serializers.CharField(
        required=True,
        max_length=100,
    )
    location_name = serializers.CharField(
        required=True,
        max_length=50,
    )
    on_sales = serializers.BooleanField(required=True)
    price = serializers.CharField(
        default='',
        allow_blank=True,
        max_length=500,
    )
    latitude = serializers.FloatField(
        allow_null=True,
        max_value=90,
        min_value=-90,
    )
    longitude = serializers.FloatField(
        allow_null=True,
        max_value=180,
        min_value=-180,
    )
    end_time = serializers.DateTimeField(required=True)

    def to_internal_value(self, data):
        taipei_tz = zoneinfo.ZoneInfo('Asia/Taipei')

        # NOTE: time
        time = datetime.strptime(data['time'], '%Y/%m/%d %H:%M:%S')
        time = time.replace(tzinfo=taipei_tz)

        # NOTE: on_sales
        on_sales = False
        if data['onSales'] == 'Y':
            on_sales = True

        # NOTE: latitude
        latitude = None
        if data['latitude']:
            latitude = float(data['latitude'])

        # NOTE: longitude
        longitude = None
        if data['longitude']:
            longitude = float(data['longitude'])

        # NOTE: end_time
        end_time = datetime.strptime(data['endTime'], '%Y/%m/%d %H:%M:%S')
        end_time = end_time.replace(tzinfo=taipei_tz)

        return {
            'id': data['show_id'] + str(data['i']),
            'show_id': data['show_id'],
            'time': time,
            'location': data['location'],
            'location_name': data['locationName'],
            'on_sales': on_sales,
            'price': data['price'],
            'latitude': latitude,
            'longitude': longitude,
            'end_time': end_time,
        }

    def create(self, validated_data):
        return ShowInfo.objects.create(**validated_data)
