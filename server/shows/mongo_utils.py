from django.conf import settings
from pymongo import MongoClient

client = MongoClient(
    username=settings.MONGO_DB_USER,
    password=settings.MONGO_DB_PASSWORD,
)
db = client[settings.MONGO_DB_NAME]
collection_name = db['shows_show']


def drop_collection():
    collection_name.drop()
    return f'{collection_name} has been dropped'


def list_all_show_item():
    show_list = collection_name.find({})
    for show_item in show_list:
        print('show_item: ', show_item['UID'])
    return None
