from celery import shared_task
import json
import time

from django.conf import settings
from pymongo import MongoClient

from shows.models import Show
from shows.serializers import (
    ShowRawSerializer,
    ShowInfoRawSerializer,
)
from shows.services import ShowService

client = None
if settings.MONGO_DB_USER and settings.MONGO_DB_PASSWORD:
    client = MongoClient(
        username=settings.MONGO_DB_USER,
        password=settings.MONGO_DB_PASSWORD,
    )
else:
    client = MongoClient()

db = client[settings.MONGO_DB_NAME]
collection_name = db['shows_show']


@shared_task(name='update_show_data')
def update_show_data():
    show_objs = Show.objects.all()
    for show_obj in show_objs:
        show_obj.delete()

    res = ShowService().fetch_data()

    # MongoDB
    try:
        collection_name.drop()
        collection_name.insert_many(res)
    except Exception as e:
        print(f'mongo operation err: {e}')

    start_time = time.time()
    for raw_show_dict in res:
        show_serializer = ShowRawSerializer(data=raw_show_dict)
        if not show_serializer.is_valid():
            print(json.dumps(show_serializer.errors, ensure_ascii=False))
        show_obj = show_serializer.create(show_serializer.validated_data)

        for i, raw_show_info_dict in enumerate(raw_show_dict['showInfo']):
            raw_show_info_dict.update({
                'show_id': show_obj.id,
                'i': i,
            })
            show_info_serializer = ShowInfoRawSerializer(
                data=raw_show_info_dict)
            if not show_info_serializer.is_valid():
                print(
                    json.dumps(show_info_serializer.errors,
                               ensure_ascii=False))
            show_info_serializer.create(show_info_serializer.validated_data)
    print(f'update_show_data: --- {time.time() - start_time} seconds ---#')
