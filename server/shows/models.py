from django.core.validators import (
    MaxValueValidator,
    MinValueValidator,
)
from django.db import models

from utils.models import CreatedAtMixin, UpdatedAtMixin


class Organizer(CreatedAtMixin, UpdatedAtMixin):
    '''單位
    
    Args:
        name: 單位名稱, ex: NTSO國立臺灣交響樂團
    '''
    name = models.CharField(
        max_length=30,
        unique=True,
    )

    def __str__(self):
        return f'{self.name}'


class Show(CreatedAtMixin, UpdatedAtMixin):
    '''活動資訊
    
    Args:
        id: UID: 唯一辨識碼
        version: 發行版本
        title: 活動名稱
        category: 活動類別
        show_unit: 演出單位
        discount_info: 折扣資訊
        description_filter_html: 簡介說明
        image_url: 圖片連結
        master_unit: 主辦單位
        sub_unit: 協辦單位
        support_unit: 贊助單位
        other_unit: 其他單位
        web_sales: 售票網址
        source_web_promote: 推廣網址
        comment: 備註
        edit_modify_date: 編輯時間
        source_web_name: 來源網站名稱
        start_date: 活動起始日期
        end_date: 活動結束日期
        hit_rate: 點閱數
    '''

    class Category(models.TextChoices):
        # TODO: change 'CATEGORY_1' into the actual name of the category
        CATEGORY_1 = '1', 'CATEGORY_1'

    id = models.CharField(
        primary_key=True,
        max_length=30,
    )
    version = models.CharField(max_length=5)
    title = models.CharField(max_length=100)
    category = models.CharField(
        max_length=1,
        choices=Category.choices,
    )
    show_unit = models.CharField(
        max_length=200,
        blank=True,
        default='',
    )
    discount_info = models.CharField(
        max_length=1000,
        blank=True,
        default='',
    )
    description_filter_html = models.CharField(
        max_length=3000,
        blank=True,
        default='',
    )
    image_url = models.URLField(
        blank=True,
        default='',
    )
    master_unit = models.ManyToManyField(
        Organizer,
        related_name='show_master_organizer',
        blank=True,
    )
    sub_unit = models.ManyToManyField(
        Organizer,
        related_name='show_sub_organizer',
        blank=True,
    )
    support_unit = models.ManyToManyField(
        Organizer,
        related_name='show_support_organizer',
        blank=True,
    )
    other_unit = models.ManyToManyField(
        Organizer,
        related_name='show_other_organizer',
        blank=True,
    )
    web_sales = models.URLField(
        blank=True,
        default='',
    )
    source_web_promote = models.URLField(
        blank=True,
        default='',
    )
    comment = models.CharField(
        max_length=300,
        blank=True,
        default='',
    )
    edit_modify_date = models.DateTimeField(
        blank=True,
        null=True,
    )
    source_web_name = models.CharField(max_length=30)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    hit_rate = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(0)],
    )

    def __str__(self):
        return f'{self.title}'


class ShowInfo(CreatedAtMixin, UpdatedAtMixin):
    '''活動場次資訊
    
    Args:
        id: Show.id + index(), ex: 64dd345d73f77c2b484e66d30
        show: Show object
        time: 單場次演出時間
        location: 地址
        location_name: 場地名稱
        on_sales: 是否售票
        price: 售票說明
        latitude: 緯度
        longitude: 經度
        end_time: 結束時間
    '''

    id = models.CharField(
        primary_key=True,
        max_length=35,
    )
    show = models.ForeignKey(Show, on_delete=models.CASCADE)
    time = models.DateTimeField()
    location = models.CharField(max_length=100)
    location_name = models.CharField(max_length=50)
    on_sales = models.BooleanField()
    price = models.CharField(
        max_length=500,
        blank=True,
        default='',
    )
    latitude = models.FloatField(
        blank=True,
        null=True,
        validators=[MinValueValidator(-90),
                    MaxValueValidator(90)],
    )
    longitude = models.FloatField(
        blank=True,
        null=True,
        validators=[MinValueValidator(-180),
                    MaxValueValidator(180)],
    )
    end_time = models.DateTimeField()

    def __str__(self):
        return f'{self.location_name} {self.id}'
