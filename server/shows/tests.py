from datetime import datetime
import json
import zoneinfo

from django.test import TestCase

from shows.models import (
    Organizer,
    Show,
)
from shows.serializers import (
    ShowRawSerializer,
    ShowInfoRawSerializer,
)


class ShowTest(TestCase):
    '''Show basic testing

    CRUD
    '''

    def setUp(self):
        # Initial Organizer
        organizer_data = {
            'name': 'NTSO國立臺灣交響樂團',
        }
        self.organizer_obj = Organizer.objects.create(**organizer_data)

    def test_create(self):
        RAW_SHOW_DICT = {
            "version":
                "1.4",
            "UID":
                "64dd345d73f77c2b484e66d4",
            "title":
                "NTSO C大調的謳歌─霍內克與國臺交",
            "category":
                "1",
            "showInfo": [{
                "time": "2024/05/26 14:30:00",
                "location": "臺中市霧峰區中正路738之2號",
                "locationName": "國立臺灣交響樂團",
                "onSales": "Y",
                "price": "",
                "latitude": None,
                "longitude": None,
                "endTime": "2024/05/26 16:20:00"
            }],
            "showUnit":
                "(奧地利)萊納‧霍內克",
            "discountInfo":
                "票價： 100 、300 、500 、800",
            "descriptionFilterHtml":
                "NTSO C 大調的謳歌─霍內克與國臺交\r\n\r\nRainer Honeck & NTSO\r\n\r\n【曲目】Program\r\n\r\n莫札特: 歌劇《狄托的仁慈》序曲\r\n\r\nW. A. Mozart: Overture “La Clemenza di Tito”, K. 621\r\n\r\n貝多芬: C大調第一號交響曲\r\n\r\nL. v. Beethoven: Symphony No. 1 in C Major, Op. 21\r\n\r\n布拉姆斯: C小調第一號交響曲\r\n\r\nJ. Brahms: Symphony No. 1 in C minor, Op. 68 ",
            "imageUrl":
                "",
            "masterUnit": ["NTSO國立臺灣交響樂團"],
            "subUnit": [],
            "supportUnit": [],
            "otherUnit": [],
            "webSales":
                "https://www.opentix.life/",
            "sourceWebPromote":
                "https://www.opentix.life/event/1687101076908056577",
            "comment":
                "",
            "editModifyDate":
                "",
            "sourceWebName":
                "全國藝文活動資訊系統",
            "startDate":
                "2024/05/26",
            "endDate":
                "2024/05/26",
            "hitRate":
                113
        }

        serializer = ShowRawSerializer(data=RAW_SHOW_DICT)
        if not serializer.is_valid():
            print(json.dumps(serializer.errors, ensure_ascii=False))

        show_obj = serializer.create(serializer.validated_data)
        self.assertEqual(show_obj.master_unit.first(), self.organizer_obj)


class ShowInfoTest(TestCase):
    '''ShowInfo basic testing

    CRUD
    '''

    def setUp(self):
        # Initial Organizer
        organizer_data = {
            'name': 'NTSO國立臺灣交響樂團',
        }
        self.organizer_obj = Organizer.objects.create(**organizer_data)

        # Initial Show
        self.taipei_tz = zoneinfo.ZoneInfo('Asia/Taipei')
        show_data = {
            'id':
                '64dd345d73f77c2b484e66d4',
            'version':
                '1.4',
            'title':
                'NTSO C大調的謳歌─霍內克與國臺交',
            'category':
                Show.Category.CATEGORY_1.value,
            'show_unit':
                '(奧地利)萊納‧霍內克',
            'discount_info':
                '票價： 100 、300 、500 、800',
            'description_filter_html':
                'NTSO C 大調的謳歌─霍內克與國臺交\r\n\r\nRainer Honeck & NTSO\r\n\r\n【曲目】Program\r\n\r\n莫札特: 歌劇《狄托的仁慈》序曲\r\n\r\nW. A. Mozart: Overture “La Clemenza di Tito”, K. 621\r\n\r\n貝多芬: C大調第一號交響曲\r\n\r\nL. v. Beethoven: Symphony No. 1 in C Major, Op. 21\r\n\r\n布拉姆斯: C小調第一號交響曲\r\n\r\nJ. Brahms: Symphony No. 1 in C minor, Op. 68 ',
            'image_url':
                '',
            'web_sales':
                'https://www.opentix.life/',
            'source_web_promote':
                'https://www.opentix.life/event/1687101076908056577',
            'comment':
                '',
            'edit_modify_date':
                None,
            'source_web_name':
                '全國藝文活動資訊系統',
            'start_date':
                datetime(year=2024, month=5, day=26, tzinfo=self.taipei_tz),
            'end_date':
                datetime(year=2024, month=5, day=26, tzinfo=self.taipei_tz),
            'hit_rate':
                113,
        }
        self.show_obj = Show.objects.create(**show_data)
        self.show_obj.master_unit.add(self.organizer_obj)

    def test_create(self):
        RAW_SHOWINFO_DICT = {
            "show_id": "64dd345d73f77c2b484e66d4",
            "i": 0,
            "time": "2024/05/26 14:30:00",
            "location": "臺中市霧峰區中正路738之2號",
            "locationName": "國立臺灣交響樂團",
            "onSales": "Y",
            "price": "",
            "latitude": None,
            "longitude": None,
            "endTime": "2024/05/26 16:20:00",
        }

        serializer = ShowInfoRawSerializer(data=RAW_SHOWINFO_DICT)
        if not serializer.is_valid():
            print(json.dumps(serializer.errors, ensure_ascii=False))

        show_info_obj = serializer.create(serializer.validated_data)
        self.assertEqual(show_info_obj.location, '臺中市霧峰區中正路738之2號')
