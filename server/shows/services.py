from urllib.error import HTTPError
import requests


class ShowService():

    def fetch_data(self):
        '''Get Show data.

        ref: https://data.gov.tw/dataset/6017
        '''
        URI = 'https://cloud.culture.tw/frontsite/trans/SearchShowAction.do'
        TIMEOUT = 10
        params = {'method': 'doFindTypeJ', 'category': '1'}

        res = None
        try:
            response = requests.post(
                URI,
                params=params,
                timeout=TIMEOUT,
            )
        except HTTPError as err:
            print(err)
        except requests.exceptions.Timeout:
            print(f'Timeout when connecting to {URI}')
        except requests.exceptions.ConnectionError as e:
            print(f'Cannot connect to {URI}')
        except requests.exceptions.HTTPError as e:
            print(f'HTTPError: {e}')
        else:
            if response.status_code == requests.codes.ok:
                res = response.json()
        finally:
            return res


if __name__ == '__main__':
    res = ShowService().fetch_data()
    print(res[1])
    print('-' * 30)
    print('-' * 30)
    print('-' * 30)
    print(res[1]['showInfo'][0])
