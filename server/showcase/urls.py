"""
URL configuration for showcase project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from rest_framework import permissions
from rest_framework.routers import DefaultRouter
from knox import views as knox_view
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from accounts import views as acc_view
from shows import views as show_view

schema_view = get_schema_view(
    openapi.Info(
        title="Showcase API",
        default_version='v1',
        description="Showcase API",
        contact=openapi.Contact(email="n574838382000@gmail.com"),
    ),
    public=True,
    authentication_classes=[],
    permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register(r'operation', show_view.ShowViewset, basename='show')
router.register(r'operation/(?P<show_id>\w+)/show-info',
                show_view.ShowInfoViewset,
                basename='show_info')

accounts_patterns = (
    [
        path('login/', acc_view.LoginView.as_view(), name='knox_login'),
        path('logout/', knox_view.LogoutView.as_view(), name='knox_logout'),
        path('logoutall/',
             knox_view.LogoutAllView.as_view(),
             name='knox_logoutall'),
    ],
    'accounts',
)

urlpatterns = [
    path('', include(router.urls)),
    path('accounts/', include(accounts_patterns)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns.append(
        path('swagger/',
             schema_view.with_ui('swagger', cache_timeout=0),
             name='schema-swagger-ui'))
