import environ
import sys

from ._base import *

# reading .env file
env = environ.Env()
# Take environment variables from .env file
environ.Env.read_env(os.path.join(PROJECT_ROOT, 'showcase.env'))

SECRET_KEY = env('DJANGO_SECRET_KEY')

TESTING = sys.argv[1:2] == ["test"]

DEBUG = True

if DEBUG and not TESTING:
    import socket  # only if you haven't already imported this

    hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
    INTERNAL_IPS = [ip[:-1] + '1' for ip in ips] + ['127.0.0.1', '10.0.2.2']

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
}

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '54.250.115.129']

# MongoDB
MONGO_DB_NAME = env('MONGO_DB_NAME')
MONGO_DB_USER = env('MONGO_DB_USER')
MONGO_DB_PASSWORD = env('MONGO_DB_PASSWORD')
