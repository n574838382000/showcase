from django.core.exceptions import ImproperlyConfigured

from ._base import *


def get_env_setting(setting_key):
    try:
        return os.environ[setting_key]
    except KeyError:
        error_msg = 'Missing required env variable [{}]'.format(setting_key)
        raise ImproperlyConfigured(error_msg)


SECRET_KEY = get_env_setting('DJANGO_SECRET_KEY')

ALLOWED_HOSTS = get_env_setting('ALLOWED_HOSTS').split(',')

# MongoDB
MONGO_DB_NAME = get_env_setting('MONGO_DB_NAME')
MONGO_DB_USER = get_env_setting('MONGO_DB_USER')
MONGO_DB_PASSWORD = get_env_setting('MONGO_DB_PASSWORD')
