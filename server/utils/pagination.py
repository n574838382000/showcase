from dataclasses import asdict

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from utils.parsers import ResponseObjCreator


class StandardPagination(PageNumberPagination):
    page_size = 10

    def get_paginated_response(self, data):
        data_dict = {
            'count': self.page.paginator.count,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'results': data
        }

        res = ResponseObjCreator().create(success=True)
        res = asdict(res)
        res.update({'data': data_dict})
        return Response(res)
