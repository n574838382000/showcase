from dataclasses import dataclass, field
from typing import Dict
from datetime import datetime
import zoneinfo


@dataclass
class ResponseObj():

    code: int = field(default=None)
    success: bool = field(default=False)
    data: Dict = field(default_factory=dict)
    message: str = field(default='')


class ResponseObjCreator():

    @dataclass(frozen=True)
    class Code():
        pass

    def create(self, success: bool, data_dict: dict = {}, message: str = ''):
        self.reply = ResponseObj(success=success,
                                 data=data_dict,
                                 message=message)

        return self.reply


class DateTimeParser():

    @staticmethod
    def taipei_tz_to_utc_tz(taipei_datetime: datetime) -> datetime:
        utc_tz = zoneinfo.ZoneInfo('UTC')
        taipei_tz = zoneinfo.ZoneInfo('Asia/Taipei')

        taipei_datetime.replace(tzinfo=taipei_tz)
        utc_datetime = taipei_datetime.astimezone(tz=utc_tz)

        return utc_datetime

    @staticmethod
    def utc_tz_to_taipei_tz(utc_datetime: datetime) -> datetime:
        taipei_tz = zoneinfo.ZoneInfo('Asia/Taipei')

        taipei_datetime = utc_datetime.astimezone(tz=taipei_tz)

        return taipei_datetime
