from dataclasses import asdict

from django.http import Http404
from rest_framework.views import exception_handler

from utils.parsers import ResponseObjCreator


def custom_exception_handler(exc, context):
    '''Customized exception response data

    Modified exceptions:
    Http404, status_code==401
    '''
    # Get original exception data
    response = exception_handler(exc, context)

    if isinstance(exc, Http404):
        res = ResponseObjCreator().create(success=False, message='Not found')
        response.data = asdict(res)
    if response:
        if response.status_code == 401:
            res = ResponseObjCreator().create(success=False,
                                              message='Please login first')
            response.data = asdict(res)
        elif response.status_code == 403:
            res = ResponseObjCreator().create(
                success=False, message='You do not have the permission')
            response.data = asdict(res)
    return response
