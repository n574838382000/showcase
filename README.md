# showcase  
  
## Project Description  
以 政府資料開放平台 之 音樂表演資訊 為資料來源，  
以django實作：  
1. 登入  
2. 音樂劇場表演訊息 之CRUD  
3. celery (period task)：每日凌晨一時更新資料  
(ref: https://data.gov.tw/dataset/6017)  
  
## Project Doc  
swagger: http://54.250.115.129:8000/swagger/  
  
## Project Detail  
### 登入  
登入: POST /accounts/login/  
登出: POST /accounts/logout/  
(ref: django-rest-knox: https://pypi.org/project/django-rest-knox/)  
  
### 操作頁面  
操作頁面：http://54.250.115.129/index.html   
i. 資料刪除：DELETE /show/<str: UID>/  
ii.資料查詢：GET /show/<str: UID>/  
iii.資料修改：PATCH /show/<str: UID>/  
  
### 自動更新資料  
mongodb: 也會跟著一起更新  
(ref: pymongo: https://pypi.org/project/pymongo/)  
  
### 單元測試  
python manage.py test shows  
  